<?php


namespace Diwo\Composer;


use Composer\Command\BaseCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DummyCommand extends BaseCommand
{
	protected function configure()
	{
		$this->setName('diwo:make-dummy-action');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$output->writeln('------------------ DUMMY DIWO COMPOSER COMMAND ------------------');
	}
}
