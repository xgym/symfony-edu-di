<?php


namespace Diwo\Composer;


use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\Capable;
use Composer\Plugin\PluginInterface;
use Composer\EventDispatcher\Event;
use Composer\Plugin\PreCommandRunEvent;
use Composer\Plugin\PreFileDownloadEvent;
use Composer\Plugin\CommandEvent;

class Plugin implements PluginInterface, EventSubscriberInterface, Capable
{
	/**
	 * @var Composer
	 */
	private $composer;
	/**
	 * @var IOInterface
	 */
	private $io;
	/**
	 * @var string
	 */
	private $varDir;

	public function activate(Composer $composer, IOInterface $io)
	{
		$this->composer = $composer;
		$this->io = $io;

		$dir = $this->composer->getConfig()->get('vendor-dir');
		$varDir = $dir . '/' . 'var';
		if (!file_exists($varDir)) {
			mkdir($varDir);
		}

		file_put_contents($varDir . '/composer-config.log', print_r($this->composer->getConfig()->all(), true));
		$this->varDir = $varDir;

		$composer->getEventDispatcher()->addListener('pre-command-run', function (PreCommandRunEvent $event) {
			$this->io->write('-------------------- PRE COMMAND RUN ---------------------> ' . $event->getCommand());
		});
	}

	public function getCapabilities()
	{
		return [
			'Composer\\Plugin\\Capability\\CommandProvider' => DiwoCommandProvider::class,
		];
	}

	public static function getSubscribedEvents()
	{
		return [
			/**  События плагина @see \Composer\Plugin\PluginEvents */
			// Проинициализирован composer
			'init' => [
				['onInit']
			],
			// Генерится перед скачиванием файла
			'pre-file-download' => [
				['onPreFileDownload']
			],
			// Генерится при выполнении команды
			'command' => [
				['onCommand']
			],

			/**
			 * Остальные события тут: https://getcomposer.org/doc/articles/scripts.md#event-names
			 */
		];
	}

	public function onInit(Event $event)
	{
		$this->io->write('------------------------- ON INIT HANDLER WORKS. Event name: ' . $event->getName());
	}

	public function onPreFileDownload(PreFileDownloadEvent $event)
	{
		file_put_contents($this->varDir . '/on-pre-file-download.log',
			random_int(0, PHP_INT_MAX)
			. ' -> '
			. date('H:i:s d-m-Y')
			. '. Event name: ' . $event->getName()
			. '. Processed URL: ' . $event->getProcessedUrl()
		);

		$this->io->write('------------------------- Event handler: onPreFileDownload');
	}

	public function onCommand(CommandEvent $event)
	{
		file_put_contents($this->varDir . '/on-command.log',
			random_int(0, PHP_INT_MAX)
			. ' -> '
			. date('H:i:s d-m-Y')
			. '. Event name: ' . $event->getName()
			. '. Command: ' . $event->getCommandName()
		);

		$event->getOutput()->writeln('------------------------- ...... STRING FROM EVENT SUBSCRIBER: onCommand ......');
	}
}
