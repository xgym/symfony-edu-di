<?php


namespace Diwo\Composer;


use Composer\Plugin\Capability\CommandProvider;

class DiwoCommandProvider implements CommandProvider
{
	public function getCommands()
	{
		return [
			new \Diwo\Composer\DummyCommand(),
		];
	}
}
