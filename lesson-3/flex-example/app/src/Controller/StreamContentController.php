<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as CoreController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class StreamContentController extends CoreController
{
	public function streamed($kernelRootDir)
	{
		$mediaFilePath = $kernelRootDir . '/Resources/media/video.mp4';
		$resource = fopen($mediaFilePath, 'rb');
		$step = 1024 * 10;

		$response = new StreamedResponse();
		$response->setCallback(function () use (&$resource, $step) {
			while (!feof($resource)) {
				echo fread($resource, $step);
				ob_flush();
				flush();
				usleep(100000);
			}

			fclose($resource);
		});

		return $response;
	}

	public function index($kernelRootDir)
	{
		return new Response(file_get_contents($kernelRootDir . '/Resources/view/video.html'));
	}
}
