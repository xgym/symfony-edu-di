<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class DownloadController extends AbstractController
{
	public function download($kernelRootDir)
	{
		return new BinaryFileResponse(
			$kernelRootDir . '/Resources/doc/example',
			BinaryFileResponse::HTTP_OK,
			[],
			true,
			ResponseHeaderBag::DISPOSITION_ATTACHMENT
		);
	}
}
