<?php


namespace App\Controller;


use App\Kernel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class AppController extends AbstractController
{
	private $env;

	public function index(Kernel $kernel = null)
	{
		return new JsonResponse([
			'env' => $this->env,
			'environment' => $kernel->getEnvironment(),
		]);
	}

	public function exception()
	{
		throw new \InvalidArgumentException('very bad exception');
	}

	public function view()
	{
		return [
			'param1' => 10,
			'param2' => 20,
		];
	}

	public function setEnv($value)
	{
		$this->env = $value;
	}
}
