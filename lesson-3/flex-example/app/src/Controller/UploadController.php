<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as CoreController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UploadController extends CoreController
{
	public function index($kernelRootDir)
	{
		return new BinaryFileResponse($kernelRootDir . '/Resources/view/upload.html');
	}

	public function do(Request $request, Filesystem $filesystem)
	{
		$uploadDir = $this->getParameter('kernel.project_dir') . '/var/upload';

		if (!$filesystem->exists($uploadDir)) {
			$filesystem->mkdir($uploadDir);
		}

		if (!$request->files->count()) {
			return new JsonResponse('no files');
		}

		/** @var UploadedFile $file */
		foreach ($request->files->all() as $file) {
			if (is_array($file)) {
				/** @var UploadedFile $f */
				foreach ($file as $f) {
					$f->move($uploadDir, 'diwo-symfony-edu-file-upload-example' . random_int(1000, 9999) . '.example');
				}
			} elseif ($file instanceof UploadedFile) {
				$file->move($uploadDir, 'diwo-symfony-edu-file-upload-example' . random_int(1000, 9999) . '.example');
			}
		}

		return new JsonResponse('Uploaded files count: ' . $request->files->count());
	}
}
