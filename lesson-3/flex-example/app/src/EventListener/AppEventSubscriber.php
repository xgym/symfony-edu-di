<?php


namespace App\EventListener;


use App\Controller\AppController;
use App\Kernel;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\DependencyInjection\ServiceSubscriberInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterControllerArgumentsEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;

class AppEventSubscriber implements EventSubscriberInterface, ServiceSubscriberInterface
{
	private $serviceLocator;

	public function __construct(ContainerInterface $serviceLocator)
	{
		$this->serviceLocator = $serviceLocator;
	}

	public static function getSubscribedEvents()
	{
		return [
			KernelEvents::REQUEST => 'onRequest',
			KernelEvents::CONTROLLER => 'onController',
			KernelEvents::CONTROLLER_ARGUMENTS => 'onControllerArguments',
			KernelEvents::RESPONSE => 'onResponse',
			KernelEvents::FINISH_REQUEST => 'onFinishRequest',
			KernelEvents::TERMINATE => 'onTerminate',
			KernelEvents::VIEW => 'onView',
			KernelEvents::EXCEPTION => 'onException',
		];
	}

	public function onRequest(GetResponseEvent $event)
	{
		if (!$event->isMasterRequest()) {
			return;
		}

		if ($event->getRequest()->isMethod(Request::METHOD_TRACE)) {
			$event->setResponse(new JsonResponse(debug_backtrace()));
		}
	}

	public function onController(FilterControllerEvent $event)
	{
		$controller = $event->getController();
		if ($event->isMasterRequest() && is_array($controller)) {
			[
				$intance,
				$action
			] = $controller + [null, ''];

			if ($intance instanceof AppController && $action === 'index') {
				$intance->setEnv('Hello mr anderson');
			}
		}
	}

	public function onControllerArguments(FilterControllerArgumentsEvent $event)
	{
		$controller = $event->getController();
		if ($event->isMasterRequest() && is_array($controller)) {
			[
				$intance,
				$action
			] = $controller + [null, ''];

			if ($intance instanceof AppController && $action === 'index') {
				$event->setArguments([
					$this->getKernel()
				]);
			}
		}
	}

	public function onResponse(FilterResponseEvent $event)
	{
		$event->getResponse()->headers->set('app-custom-header', 'ololo-trololo');
	}

	public function onFinishRequest(FinishRequestEvent $event)
	{
		if ($event->isMasterRequest()) {
			$this->log('Reqeust finished');
		}
	}

	public function onTerminate(PostResponseEvent $event)
	{
		if ($event->isMasterRequest()) {
			$this->log('Terminated!!!');
			file_put_contents($this->getKernel()->getProjectDir() . '/var/terminated.txt', date('H:i:s d-m-Y'));
		}
	}

	public function onView(GetResponseForControllerResultEvent $event)
	{
		$event->setResponse(new JsonResponse($event->getControllerResult()));
	}

	public function onException(GetResponseForExceptionEvent $event)
	{
		$exception = $event->getException();
		$this->log('Exception occurred: ' . $exception->getMessage());
		$event->setResponse(new JsonResponse([
			'exception' => get_class($exception),
			'message' => $exception->getMessage(),
			'code' => $exception->getCode(),
			'line' => $exception->getLine(),
			'trace' => $exception->getTrace(),
		], JsonResponse::HTTP_BAD_REQUEST));
	}

	public static function getSubscribedServices()
	{
		return [
			'kernel' => 'kernel',
			'logger' => LoggerInterface::class,
		];
	}

	private function log($message)
	{
		if (!$this->serviceLocator->has('logger')) {
			throw new ServiceNotFoundException(LoggerInterface::class);
		}

		/** @var LoggerInterface $logger */
		$logger = $this->serviceLocator->get('logger');
		$logger->info('[app.info] ' . $message);
	}

	/**
	 * @return Kernel
	 */
	private function getKernel(): Kernel
	{
		if (!$this->serviceLocator->has('kernel')) {
			throw new ServiceNotFoundException('You should provide "kernel" service to DI');
		}

		return $this->serviceLocator->get('kernel');
	}
}
