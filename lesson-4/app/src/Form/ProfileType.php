<?php

namespace App\Form;

use App\Entity\Profile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class ProfileType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('fullName', TextType::class, ['attr' => [
				'maxlength' => 255,
			]])
			->add('avatar', TextType::class)
			->add('timezone', TimezoneType::class)
			->add('birthdate', DateTimeType::class, [
				'widget' => 'single_text',
				'format' => 'd.m.Y',
				'input' => 'datetime_immutable',
				'attr' => [
					'pattern' => '[0-3][0-9]\.[01][0-9]\.20[0-9][0-9]',
				],
			])
			->add('gender', GenderType::class)
			->add('submit', SubmitType::class, [
				'label' => 'Do it!'
			])
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Profile::class,
		]);
	}
}
