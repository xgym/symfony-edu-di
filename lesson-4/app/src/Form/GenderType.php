<?php


namespace App\Form;


use App\Entity\Gender;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class GenderType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('type', ChoiceType::class, [
				'choice_loader' => new CallbackChoiceLoader(function () {
					return array_combine(Gender::genders(), Gender::genders());
				}),
				'placeholder' => 'Choose gender...',
			])
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Gender::class,
		]);
	}
}
