<?php


namespace App\Entity;


class Profile
{
	/**
	 * @var string
	 */
	private $fullName;
	/**
	 * @var string
	 */
	private $avatar;
	/**
	 * @var string
	 */
	private $timezone;
	/**
	 * @var \DateTimeImmutable
	 */
	private $birthdate;
	/**
	 * @var Gender
	 */
	private $gender;


	/**
	 * @return string
	 */
	public function getFullName(): string
	{
		return $this->fullName;
	}

	/**
	 * @param string $fullName
	 */
	public function setFullName(string $fullName): void
	{
		$this->fullName = $fullName;
	}

	/**
	 * @return string
	 */
	public function getAvatar(): string
	{
		return $this->avatar;
	}

	/**
	 * @param string $avatar
	 */
	public function setAvatar(string $avatar): void
	{
		$this->avatar = $avatar;
	}

	/**
	 * @return string
	 */
	public function getTimezone(): string
	{
		return $this->timezone;
	}

	/**
	 * @param string $timezone
	 */
	public function setTimezone(string $timezone): void
	{
		$this->timezone = $timezone;
	}

	/**
	 * @return \DateTimeImmutable
	 */
	public function getBirthdate(): \DateTimeImmutable
	{
		return $this->birthdate;
	}

	/**
	 * @param \DateTimeImmutable $birthdate
	 */
	public function setBirthdate(\DateTimeImmutable $birthdate): void
	{
		$this->birthdate = $birthdate;
	}

	/**
	 * @return Gender
	 */
	public function getGender(): Gender
	{
		return $this->gender;
	}

	/**
	 * @param Gender $gender
	 */
	public function setGender(Gender $gender): void
	{
		$this->gender = $gender;
	}
}
