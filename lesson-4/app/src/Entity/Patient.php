<?php


namespace App\Entity;


class Patient
{
	/**
	 * @var Profile
	 */
	private $profile;
	/**
	 * @var CardNumber
	 */
	private $cardNumber;
	/**
	 * @var \DateTimeImmutable
	 */
	private $created;
	/**
	 * @var string
	 */
	private $allergy;

	/**
	 * @return Profile
	 */
	public function getProfile(): ?Profile
	{
		return $this->profile;
	}

	/**
	 * @param Profile $profile
	 * @return Patient
	 */
	public function setProfile(Profile $profile): self
	{
		$this->profile = $profile;

		return $this;
	}

	/**
	 * @return CardNumber
	 */
	public function getCardNumber(): CardNumber
	{
		return $this->cardNumber;
	}

	/**
	 * @param CardNumber $cardNumber
	 * @return Patient
	 */
	public function setCardNumber(CardNumber $cardNumber): self
	{
		$this->cardNumber = $cardNumber;

		return $this;
	}

	/**
	 * @return \DateTimeImmutable
	 */
	public function getCreated(): \DateTimeImmutable
	{
		return $this->created;
	}

	/**
	 * @param \DateTimeImmutable $created
	 * @return Patient
	 */
	public function setCreated(\DateTimeImmutable $created): self
	{
		$this->created = $created;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAllergy(): string
	{
		return $this->allergy;
	}

	/**
	 * @param string $allergy
	 * @return Patient
	 */
	public function setAllergy(string $allergy): self
	{
		$this->allergy = $allergy;

		return $this;
	}
}
