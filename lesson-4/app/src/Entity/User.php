<?php


namespace App\Entity;


class User
{
	/**
	 * @var string
	 */
	private $login;
	/**
	 * @var string
	 */
	private $password;
	/**
	 * @var \DateTimeImmutable
	 */
	private $created;
	/**
	 * @var \DateTimeImmutable
	 */
	private $banned;
	/**
	 * @var string
	 */
	private $role;

	/**
	 * @return string
	 */
	public function getLogin(): string
	{
		return $this->login;
	}

	/**
	 * @param string $login
	 */
	public function setLogin(string $login): void
	{
		$this->login = $login;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword(string $password): void
	{
		$this->password = $password;
	}

	/**
	 * @return \DateTimeImmutable
	 */
	public function getCreated(): \DateTimeImmutable
	{
		return $this->created;
	}

	/**
	 * @param \DateTimeImmutable $created
	 */
	public function setCreated(\DateTimeImmutable $created): void
	{
		$this->created = $created;
	}

	/**
	 * @return \DateTimeImmutable
	 */
	public function getBanned(): \DateTimeImmutable
	{
		return $this->banned;
	}

	/**
	 * @param \DateTimeImmutable $banned
	 */
	public function setBanned(\DateTimeImmutable $banned): void
	{
		$this->banned = $banned;
	}

	/**
	 * @return string
	 */
	public function getRole(): string
	{
		return $this->role;
	}

	/**
	 * @param string $role
	 */
	public function setRole(string $role): void
	{
		$this->role = $role;
	}
}
