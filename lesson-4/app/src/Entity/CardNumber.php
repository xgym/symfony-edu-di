<?php


namespace App\Entity;


class CardNumber
{
	/**
	 * @var string
	 */
	private $series;
	/**
	 * @var string
	 */
	private $number;
	/**
	 * @var \DateTimeImmutable
	 */
	private $registered;

	/**
	 * @return string
	 */
	public function getSeries(): string
	{
		return $this->series;
	}

	/**
	 * @param string $series
	 */
	public function setSeries(string $series): void
	{
		$this->series = $series;
	}

	/**
	 * @return string
	 */
	public function getNumber(): string
	{
		return $this->number;
	}

	/**
	 * @param string $number
	 */
	public function setNumber(string $number): void
	{
		$this->number = $number;
	}

	/**
	 * @return \DateTimeImmutable
	 */
	public function getRegistered(): \DateTimeImmutable
	{
		return $this->registered;
	}

	/**
	 * @param \DateTimeImmutable $registered
	 */
	public function setRegistered(\DateTimeImmutable $registered): void
	{
		$this->registered = $registered;
	}
}
