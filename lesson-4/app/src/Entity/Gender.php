<?php


namespace App\Entity;


class Gender
{
	const MALE = 'male';
	const FEMALE = 'female';
	const UNDEFINED = 'undefined';
	const IT = 'it';
	const NULLED = 'nulled';
	const MIDDLE = 'middle';

	private $type;

	public function __construct($gender)
	{
		if (!in_array($gender, self::genders())) {
			throw new \LogicException('Invalid gender');
		}

		$this->type = $gender;
	}

	public static function genders() 
	{
		return [
			self::MALE,
			self::FEMALE,
			self::UNDEFINED,
			self::IT,
			self::NULLED,
			self::MIDDLE,
		];
	}

	public function type()
	{
		return $this->type;
	}
}
