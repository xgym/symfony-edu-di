<?php


namespace App\Controller;


use App\Form\InsuranceType;
use App\Form\ProfileType;
use App\Form\ScheduleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class ProfileController extends AbstractController
{
	public function create()
	{
		return $this->render('profile/create.html.twig', [
			'profile_form' => $this->createForm(ProfileType::class)->createView(),
		]);
	}

	public function inherit()
	{
		return $this->render('profile/inherit.html.twig', [
			'schedule_form' => $this->createForm(ScheduleType::class)->createView(),
			'insurance_form' => $this->createForm(InsuranceType::class)->createView(),
		]);
	}

	public function custom(Request $request)
	{
		$form = $this
			->createFormBuilder()
				->add(
					'name',
					TextType::class,
					[
						'label' => 'Имя пользователя',
						'attr' => [
							'data-content' => 'some value',
							'placeholder' => 'Имя пользователя'
						],
						'constraints' => [
							new NotBlank(),
							new Length([
								'min' => 5,
								'max' => 30,
							])
						],
					]
				)
				->add(
					'age',
					NumberType::class,
					[
						'label' => 'Возраст пользователя',
						'attr' => [
							'placeholder' => 'Возраст пользователя',
						],
						'constraints' => [
							new NotBlank(),
							new Range([
								'min' => 0,
								'max' => 200,
							])
						],
					]
				)
				->add('submit', SubmitType::class)
			->getForm()
		;

		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			return $this->render('profile/custom-result.html.twig', [
				'data' => $form->getData(),
			]);
		}

		return $this->render('profile/custom.html.twig', [
			'form' => $form->createView(),
		]);
	}
}
