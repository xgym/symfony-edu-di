# Lesson 4

## Forms

1. То что мы называем контрол - у Symfony называется Type.
1. Types которые идут в стандартной поставке: https://symfony.com/doc/current/reference/forms/types.html
1. Механизм Form Extensions? Расширяет стандартные возможности FormType's
1. Обработка запроса формой: \Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationRequestHandler
1. Имеется собственный compiler pass: \Symfony\Component\Form\DependencyInjection\FormPass: производит конфигурирование отладочной команды для форм: `debug:form` - которая покажет всю информацию о зареганных типах и остальных составляющих форм в проекте.
1. Любой тип можно расширить, реализовав следующий интерфейс: \Symfony\Component\Form\FormTypeExtensionInterface
	1. Определить действия при build form, например \Symfony\Component\Form\Extension\Csrf\Type\FormTypeCsrfExtension::buildForm - вешает собственный лисенер на событие preSubmit
	1. Кастомизировать build view - примеров нет пока
	1. Кастомизировать finish view - \Symfony\Component\Form\Extension\Csrf\Type\FormTypeCsrfExtension::finishView - добавляет свое поле для проверки токена csrf.
	1. ConfigureOptions установить глобально опции для типа формы - \Symfony\Component\Form\Extension\Csrf\Type\FormTypeCsrfExtension::configureOptions
1. Расширения по умолчанию:
	1. Symfony\Component\Form\Extension\HttpFoundation\Type\FormTypeHttpFoundationExtension
	1. Symfony\Component\Form\Extension\Validator\Type\FormTypeValidatorExtension
	1. Symfony\Component\Form\Extension\Validator\Type\RepeatedTypeValidatorExtension
	1. Symfony\Component\Form\Extension\Validator\Type\SubmitTypeValidatorExtension
	1. Symfony\Component\Form\Extension\Validator\Type\UploadValidatorExtension
1. Общий флоу работы с формой:
	1. Описать новый Type
	1. Подготовить шаблон
	1. Добавить экшн в контроллере
1. Events:
	1. \Symfony\Component\Form\Form::setData (pre_set_data, post_set_data)
	1. \Symfony\Component\Form\Form::submit (pre_submit, submit, post_submit)

## Twig

Функции для работы с формой в шаблоне https://symfony.com/doc/current/reference/forms/twig_reference.html
