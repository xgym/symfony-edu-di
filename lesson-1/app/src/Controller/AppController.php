<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AppController
{
	/**
	 * @Route("/home",methods={"GET"})
	 */
	public function index()
	{
		$data = new \stdClass();
		$data->app = 'dwteam/Symfony-EDU';
		$data->status = 'ok';
		$data->now = new \DateTimeImmutable();
		$data->action = 'index';

		return new JsonResponse($data);
	}

	/**
	 * @Route("/now",methods={"GET"})
	 */
	public function now()
	{
		return new JsonResponse(new \DateTimeImmutable());
	}
}
