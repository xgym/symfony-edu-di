<?php


namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HtmlController
{
	public function id($id)
	{
		return new Response(sprintf('<h1>Your ID: %s</h1>', $id));
	}

	public function index(Request $request)
	{
		return new Response('<h1>Hello mr anderson...</h1>');
	}
}
