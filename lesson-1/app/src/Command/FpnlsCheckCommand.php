<?php


namespace App\Command;


use App\Provider\Fplns\Envelope\InputCheckEnvelope;
use App\Provider\Fplns\FplnsProviderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FpnlsCheckCommand extends Command
{
	/**
	 * @var FplnsProviderInterface
	 */
	private $provider;

	public function __construct(FplnsProviderInterface $provider)
	{
		parent::__construct(null);
		$this->provider = $provider;
	}

	protected function configure()
	{
		$this->setName('symfony-edu:fpnls-check');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$result = $this->provider->check(new InputCheckEnvelope());

		$output->writeln('Result is: ' . serialize($result));
	}
}
