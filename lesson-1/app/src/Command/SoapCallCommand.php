<?php


namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SoapCallCommand extends Command
{
	/**
	 * @var \SoapClient
	 */
	private $client;
	/**
	 * @var \SoapClient
	 */
	private $client2;

	public function __construct(\SoapClient $soapClientTest8)
	{
		parent::__construct(null);
		$this->client2 = $soapClientTest8;
	}


	protected function configure()
	{
		$this->setName('symfony-edu:soap-call');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->client->call();
	}

	/**
	 * @required
	 * @param \SoapClient $soapClientTest7
	 */
	public function setSoapClient(\SoapClient $soapClientTest7)
	{
		$this->client = $soapClientTest7;
	}
}
