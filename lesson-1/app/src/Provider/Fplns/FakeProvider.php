<?php


namespace App\Provider\Fplns;


use App\Provider\Fplns\Envelope\InputCheckEnvelope;
use App\Provider\Fplns\Envelope\ResponseEnvelope;

class FakeProvider implements FplnsProviderInterface
{
	public function check(InputCheckEnvelope $checkEnvelope): ResponseEnvelope
	{
		$response = new ResponseEnvelope();
		$response->Result = true;

		return $response;
	}
}
