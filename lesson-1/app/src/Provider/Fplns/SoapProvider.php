<?php


namespace App\Provider\Fplns;


use App\Provider\Fplns\Envelope\InputCheckEnvelope;
use App\Provider\Fplns\Envelope\ResponseEnvelope;

class SoapProvider implements FplnsProviderInterface
{
	/**
	 * @var \SoapClient
	 */
	private $soapClient;

	public function __construct(\SoapClient $soapClientFpnls)
	{
		$this->soapClient = $soapClientFpnls;
	}

	public function check(InputCheckEnvelope $inputCheckEnvelope): ResponseEnvelope
	{
		// let's imagine we have mapping on our ResponseEnvelope
		return $this->soapClient->check($inputCheckEnvelope);
	}
}
