<?php


namespace App\Provider\Fplns;


use App\Provider\Fplns\Envelope\InputCheckEnvelope;
use App\Provider\Fplns\Envelope\ResponseEnvelope;

interface FplnsProviderInterface
{
	public function check(InputCheckEnvelope $checkEnvelope): ResponseEnvelope;
}
