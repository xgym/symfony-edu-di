<?php


namespace SymfonyEdu\Bundle\SoapClient\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DebugSoapClientCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this->setName('symfony-edu:soap-client:list');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->getContainer()->get('soap_client.test1')->break();
	}
}
