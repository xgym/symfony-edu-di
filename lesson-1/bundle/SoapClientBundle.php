<?php


namespace SymfonyEdu\Bundle\SoapClient;


use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use SymfonyEdu\Bundle\SoapClient\DependencyInjection\CompilerPass\BindingClientsCompilerPass;
use SymfonyEdu\Bundle\SoapClient\DependencyInjection\SoapClientExtension;

class SoapClientBundle extends Bundle
{
	public function build(ContainerBuilder $container)
	{
		$container->addCompilerPass(new BindingClientsCompilerPass(), PassConfig::TYPE_OPTIMIZE, 1);
		$container->registerExtension(new SoapClientExtension());
	}
}
