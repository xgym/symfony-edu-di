<?php


namespace SymfonyEdu\Bundle\SoapClient\DependencyInjection\CompilerPass;


use App\Command\SoapCallCommand;
use Symfony\Component\DependencyInjection\Argument\BoundArgument;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use SymfonyEdu\Bundle\SoapClient\DependencyInjection\SoapClientExtension;

class BindingClientsCompilerPass implements CompilerPassInterface
{
	public function process(ContainerBuilder $container)
	{
		$bindings = [];
		$clients = $container->findTaggedServiceIds(SoapClientExtension::TAG);
		foreach ($clients as $id => $definition) {
			$bindings[$this->bindingName($id)] = $id;
		}

		if (!$bindings) {
			return;
		}

		foreach ($container->getDefinitions() as $id => $definition) {
			if (
				$definition->hasTag(SoapClientExtension::TAG)
				|| $definition->isAbstract()
				|| !$definition->getClass()
			) {
				continue;
			}

			$serviceBindings = $definition->getBindings();
			foreach ($bindings as $binding => $soapClientId) {
				$boundArgument = new BoundArgument(new Reference($soapClientId));
				[$value, $identifier] = $boundArgument->getValues();
				$boundArgument->setValues([$value, $identifier, true]);
				$serviceBindings[$binding] = $boundArgument;
			}

			$definition->setBindings($serviceBindings);
		}
	}

	private function bindingName($clientId)
	{
		return '$' . lcfirst(
				implode('',
					array_map(
						'ucfirst',
						preg_split('/[_\.]/i', $clientId)
					)
				)
			);
	}
}
