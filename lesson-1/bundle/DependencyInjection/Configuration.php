<?php


namespace SymfonyEdu\Bundle\SoapClient\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
	public function getConfigTreeBuilder()
	{
		$treeBuilder = new TreeBuilder();
		$rootNode = $treeBuilder->root('soap_client');
		$rootNode
			->beforeNormalization()
				->ifTrue(function ($v) {
					return (
						is_array($v)
						&& array_key_exists('clients', $v)
						&& array_key_exists('default', $v)
						&& is_array($v['clients'])
						&& !array_key_exists($v['default'], $v['clients'])
					);
				})
				->thenInvalid('default: Unknown client name')
			->end()
			->children()
				->scalarNode('default')->isRequired()->end()
				->arrayNode('clients')
					->useAttributeAsKey('name')
					->cannotBeEmpty()
					->isRequired()
					->prototype('array')
						->children()
							->scalarNode('wsdl')->end()
							->arrayNode('options')
								->useAttributeAsKey('name')
								->prototype('array')
									->children()
										->scalarNode('soap_version')->end()
										->booleanNode('exception')->defaultTrue()->isRequired()->end()
										->scalarNode('connection_timeout')->defaultValue(5)->end()
										->scalarNode('cache_wsdl')->end()
										->arrayNode('stream_context')
											->useAttributeAsKey('name')
											->prototype('array')
												->children()
													->scalarNode('local_cert')->end()
													->scalarNode('passphrase')->end()
													->booleanNode('verify_peer')->defaultFalse()->end()
													->booleanNode('verify_peer_name')->defaultFalse()->end()
												->end()
											->end()
										->end()
									->end()
								->end()
							->end()
						->end()
					->end()
				->end()
			->end();

		return $treeBuilder;
	}
}
