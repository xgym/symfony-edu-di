<?php


namespace SymfonyEdu\Bundle\SoapClient\DependencyInjection;


use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class SoapClientExtension extends Extension
{
	const TAG = 'soap_client';

	public function load(array $configs, ContainerBuilder $container)
	{
		$this->loadConfigs($container);
		$this->registerClients($configs, $container);
	}

	private function loadConfigs(ContainerBuilder $container)
	{
		$loader = new YamlFileLoader($container, new FileLocator(\dirname(__DIR__) . '/Resources/config'));

		$loader->load('console.yaml');
		$loader->load('client.yaml');
	}

	private function registerClients(array $configs, ContainerBuilder $container)
	{
		$configs = $configs[0] ?? [];
		if (!array_key_exists('clients', $configs)) {
			return;
		}

		$defaultClient = $configs['default'];
		foreach ($configs['clients'] as $name => $client) {
			$wsdl = $client['wsdl'];
			$options = (array)($client['options'] ?? []);

			$clientDefinition = $this->createClientDefinition($wsdl, $options);
			$clientId = 'soap_client.' . $name;
			$container->setDefinition($clientId, $clientDefinition);

			if ($name === $defaultClient) {
				$container->setAlias('soap_client.default', $clientId);
			}
		}
	}

	private function createClientDefinition(string $wsdl, array $options)
	{
		$clientDefinition = new ChildDefinition('soap_client.abstract');
		$clientDefinition->replaceArgument(0, $wsdl);
		$clientDefinition->replaceArgument(1, $options);
		$clientDefinition->addTag(self::TAG);

		return $clientDefinition;
	}
}
