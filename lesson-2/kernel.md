# Symfony-edu/lesson-2

```
\App\Kernel
	Конфигруационные файлы: загружаются с учетом окружения
		1) Загрузка конфигурационных файлов - бандлов и сервисов
			\App\Kernel::configureContainer
		2) Загрузка конфигов для роутинга
			\App\Kernel::configureRoutes
```

## Создание объекта Request

Метод вызывается перед $kernel->handle:

`\Symfony\Component\HttpFoundation\Request::createFromGlobals`

Внутри происходит вызов `\Symfony\Component\HttpFoundation\Request::createRequestFromFactory`

Куда прокидываются глобальные переменные $_GET, $_POST, $_FILES, $_COOKIES, $_SERVER.

`\Symfony\Component\HttpFoundation\Request::setFactory` - можно определить коллбэк и переопределить способ создания объекта.

## Запуск приложения (http)

1. `$kernel->handle()`
	1. `$kernel->boot()`
		1. `\Symfony\Component\HttpKernel\Kernel::initializeBundles`
			1. `\App\Kernel::registerBundles` - происходит создание инстансов классов `\Symfony\Component\HttpKernel\Bundle\BundleInterface`
		1. `\Symfony\Component\HttpKernel\Kernel::initializeContainer`
			1. Начинается инициализация контейнера
				1. Делается попытка загрузить из кэша (для конфига кэша используется объект `\Symfony\Component\Config\ConfigCache`)
				1. В случае неудачи:
					1. Билдится новый котейнер
					1. Выполняется его компиляция
					1. Если имеется экземпляр старого контейнера - он сохраняется на случай, если выполняются запросы со старым
					1. Выполняется прогрев кэша:
						1. Можно зарегать свой собственный сервис прогрева, нужно реализовать `\Symfony\Component\HttpKernel\CacheWarmer\WarmableInterface`
						1. По умолчанию зареганы следующие сервисы:
							1. \Symfony\Bundle\FrameworkBundle\CacheWarmer\RouterCacheWarmer
							1. \Symfony\Bundle\FrameworkBundle\CacheWarmer\TranslationsCacheWarmer
							1. \Symfony\Bundle\FrameworkBundle\CacheWarmer\TemplatePathsCacheWarmer
							1. \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerAggregate
		1. `\Symfony\Component\HttpKernel\Bundle\BundleInterface::boot`

## Общий flow

В приложении есть 2 "ядра":

1. \App\Kernel - сердце приложения, управляет окружением, которое созданно бандлами
1. \Symfony\Component\HttpKernel\HttpKernel - управляет жизненным циклоа "Запрос - ответ"
1. 1е вызывает 2е.

Любое веб приложение отвечает на запросы. Непосредственно за сам процесс отвечает специальный компонент HttpKernel.

https://symfony.com/doc/current/components/http_kernel.html

Более детально по шагам этот процесс выглядит так:

1. Пользователь запрашивает ресурс в браузере.
1. Браузер отправляет запрос на сервер.
1. Symfony передает приложение инстанс объекта Request (запрос)
1. Приложение генерит инстанс объекта Response используя данные Request
1. Сервер отправляет назад ответ браузеру
1. Браузер отображает ресурс пользователю

Компонент HttpKernel предоставляет интерфейс который формализует процесс от начала запроса до формирования ответа.

Во время обработки запроса этот компонент выбрасывает ряд событий, на которые можно подписываться.

##### Пример

Открыть index.php

Symfony\Component\HttpKernel\HttpKernel::handle

Далее перейти к методу handleRaw и показать весь жизненный цикл приложения. Во время handle случается 4 события:

1. `\Symfony\Component\HttpKernel\KernelEvents::REQUEST` - событие случает каждый раз вначале обработки запроса, например на это событие подписываются специальный лисенеры от компонента Security, которые выполняют свои проверки и могут обломить с доступом. На этом этапе можно создать объект Response и вернуть его, тогда приложение перейдет к отправке ответа сразу. Можно добавлять инфу к запросу. Другой лисенер - это роутер, он определяет какой контроллер должен обработать запрос. При установке Response дальнейшее распространение события прекращается и все низкоприоритетные лисенеры обламываются.
1. `\Symfony\Component\HttpKernel\KernelEvents::CONTROLLER` - событие происходит после того как найден соответствующий контроллер. Инстанцированием контроллера занимается специальный класс \Symfony\Component\HttpKernel\Controller\ControllerResolverInterface - он может как создавать инстанс, так и обращаться к контейнеру (по-умолчанию - все контроллеры сервисы). Здесь можно подменить контроллер, например, или продолжить инициализцию каких-либо сервисов (например если важна информация о роутинге или других сервисов которые отработали ранее). На это событие завязана работа анотации @ParamConverter. В этот момент известен контроллер, но неизвестны аргументы.
1. `\Symfony\Component\HttpKernel\KernelEvents::CONTROLLER_ARGUMENTS` - получены все аргументы экшена контроллера. В этот момент закончил работу ArgumentResolver - который прочитал сигнатуру экшена и можно прокинуть соответствующие аргументы в экшн. Этот резолвер смотрит в данные запроса, на подсказки типов (DI), либо variadic из запроса (...$args). Можно создать свои резолверы.
1. `\Symfony\Component\HttpKernel\KernelEvents::VIEW` - вызывается если контроллер вернул ответ отличный от Response класса. Например анотация @Template - она из SensioFrameworkExtraBundle и при ее использовании из контроллера можно возвращать любые данные, а она уже разрулит какой шаблон юзать (именование шаблонов важно).
1. `\Symfony\Component\HttpKernel\KernelEvents::RESPONSE` - вызывается когда сформирован ответ, перед отправкой. \Symfony\Component\HttpKernel\HttpKernel::filterResponse. На этом этапе можно дополнить или заменить ответ. Например, можно добавить дополнительную отладочную инфу или операции с сессией, установка кук.
1. `\Symfony\Component\HttpKernel\KernelEvents::FINISH_REQUEST` - событие случается когда ответ был полностью сформирован, иначе - когда отработали все лисенеры на событие ::RESPONSE. Ответ тут уже недоступен.
1. `\Symfony\Component\HttpKernel\KernelEvents::TERMINATE` - случается когда ответ был отправлен, в основном используется для обработки тяжелых и долгих действий, когда все уже ушло пользователю. Например отправка писем (memory spooling). Использует fastcgi_finish_request, что означает - будет крутиться FPM процесс пока операции не закончатся.
1. `\Symfony\Component\HttpKernel\KernelEvents::EXCEPTION` - случается, когда выброшен эксепшн в каком-либо шаге. Лисенеры этого события могут формировать соответствующий ответ.

Все события подробно описаны в файле \Symfony\Component\HttpKernel\KernelEvents

Чтобы создать лисенер - нужно создать класс и в контейнере пометить его тегом kernel.event_listener, указав метод и событие:
```yaml
services:
	app.ololo_trololo_event_listener:
		class: Foo\Bar\Ololo\Trololo
		tags:
			- {name: 'kernel.event_listener', event:'ollolo_trololo.event', method:'onOlloloTrololoEvent'}
```

Другой путь `\Symfony\Component\EventDispatcher\EventSubscriberInterface` - реализовав для класса этот интерфейс, контейнер сам его зарегает.

## Подзапрос

\Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait::forward

Внутри приложения можно выполнить подзапрос, при этом будет отличаться Request - он больше не будет master (по умолчанию, но это можно переопределить). Если будет master request тип запроса - то могут опять сработать основные лисенеры, как правило в них есть эта проверка.

## Поиск ресурсов

Kernel может искать реальные пути к файлам конфигам бандлов (см. \Symfony\Component\HttpKernel\KernelInterface::locateResource). 

$pathToConfig = $kernel->locateResource('@Ololo/Trololo/Resources/config/services.yaml');
