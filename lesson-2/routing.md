# Маршрутизация

За маршрутизацию отвечает компонент https://symfony.com/doc/current/components/routing.html

Маршрутизация использует 3 основных сервиса:

* RouteCollection - коллекция определений маршрутов
* RequestContext - контекст текущего запроса
* UrlMatcher - сервис поиска подходящего маршрута для запроса

Для формирования коллекции маршрутов используются специальные классы, реализующие интерфейс \Symfony\Component\Config\Loader\LoaderInterface.

Эти классы реализованы в самом компонент Routing, внутри они в зависимости от формата разбирают конфиги и возвращают RouteCollection.

В самом фреймворке есть специальный сервис \Symfony\Bundle\FrameworkBundle\Routing\Router, который делает следующее:

Конфиг компонента лежит ./app/config/packages/routing.yaml

```yaml
framework:
	router:
		enabled:              false
		resource:             ~ # by default: kernel::loadRoutes
		type:                 ~ # php|yaml|xml|annotation|glob|directory|closure|service
		http_port:            80
		https_port:           443
		strict_requirements:  true # строгая проверка placeholder'ов в маршрутах
```

В конфигурации можно указать сервис и метод для загрузки маршрутов.

При запросе \Symfony\Component\HttpKernel\EventListener\RouterListener - устанавливает контроллер и параметры маршрута в master request.

Конфигурирование маршрутов происходит при подогреве кэша.

Используемая реализация \Symfony\Component\HttpKernel\Controller\ControllerResolverInterface на основании атрибута запроса _controller - вызвает нужные экшн.


### Полезные команды:

php bin/console config:dump-reference framework
php bin/console debug:config framework
php bin/console debug:router
php bin/console debug:event-dispatcher

По умолчанию загрузка роутов происходит этим методом \Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait::loadRoutes

Для работы с аннотациями нужен пакет annotations (doctrine)

Ядро приложения грузит конфиги из папки config. Конфиг может иметь следующий формат:

```yaml
routes_collection_name:
	resource: 'some/glob/pattern/or/path/to/any/controller/class'
	type: 'annotation'
	prefix: '/api/v1'
	name_prefix: 'custom_name_'
	condition: 'true' # required expression component, here we have instance of RequestContext and Request
	methods: []
	schemes: []
	options:
		compiler_class: Some\CompilerClass
		utf8: true|false
	trailing_slash_on_root: false
	requirements: {id: "[0-9]+", name: "[a-z]{1,10}"}

any_route_name:
	path: 'route/uri'
	controller: App\Foo\Controller::index
	defaults: {_controller: 'App\Foo\Controller::index', id: 10, name: OloloTrololo}
	methods: []
    schemes: []
    condition: ''
    options:
        compiler_class: Some\CompilerClass
        utf8: true|false
    requirements: {id: "[0-9]+"} 
	
```

При разогреве кэша получается вот такой класс: var/cache/dev/srcDevDebugProjectContainerUrlMatcher.php

Компиляцией маршрутов занимается PhpMatchDumper - он генерит класс с готовыми роутами, происходит это так:

1. PhpMatcherDumper.php:213
1. Route.php:548

